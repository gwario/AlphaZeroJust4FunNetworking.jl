# AlphaZeroJust4FunNetworking [![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://gwario.gitlab.io/AlphaZeroJust4FunNetworking.jl/dev) [![Build Status](https://gitlab.com/gwario/AlphaZeroJust4FunNetworking.jl/badges/main/pipeline.svg)](https://gitlab.com/gwario/AlphaZeroJust4FunNetworking.jl/pipelines) [![Coverage](https://gitlab.com/gwario/AlphaZeroJust4FunNetworking.jl/badges/main/coverage.svg)](https://gitlab.com/gwario/AlphaZeroJust4FunNetworking.jl/commits/main)

This package aims to integrate AlphaZeroJust4Fun with external games, separate AlphaZero instances and external players.

## Design

There are three types of components. The game coordinator, game providers and network client players.

### Game Coordinator

Synchronizes the state of the game (stones, used cards and own cards) from the game provider. Provides the game state for the player clients. Receives actions from the player clients.

### Providers

Provides the state of the game (stones, used cards and own cards) for the game coordinator. Applies state changes, imposed by the game coordinator, to the game.

#### Web Just4Fun Provider

Provides the state of a web based game and applies player actions.

#### Local Just4Fun Provider

Creates and provides the state of a local game and applies player actions.

### Network Client Player

Connects to a game coordinator and waits for the game start. Receives partial game states (stones, used cards, own cards) and sends player actions.
To be used in a duel against an honest local player (i.e. an AlphaZero, MCTS, NetworkOnly, ...).

## HOW TO

* First start the players and then the game provider.

