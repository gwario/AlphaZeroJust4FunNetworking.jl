using AlphaZeroJust4FunNetworking
using Documenter

DocMeta.setdocmeta!(AlphaZeroJust4FunNetworking, :DocTestSetup, :(using AlphaZeroJust4FunNetworking); recursive=true)

makedocs(;
    modules=[AlphaZeroJust4FunNetworking],
    authors="Mario Gastegger <mario.gastegger@gmail.com>",
    repo="https://gitlab.com/gwario/AlphaZeroJust4FunNetworking.jl/blob/{commit}{path}#{line}",
    sitename="AlphaZeroJust4FunNetworking.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://gwario.gitlab.io/AlphaZeroJust4FunNetworking.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
