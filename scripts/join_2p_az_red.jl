using Revise

using AlphaZeroJust4FunNetworking
using AlphaZeroJust4Fun
using Just4Fun
using AlphaZero
using AlphaZero: TrainingSample, GI, Benchmark, UI


const USE_GPU = true

@info "Creating local game template..."
gspec = Just4FunSpec()

architecture =  Architectures.ResNet_v1_0
parameters = Parameters.ResNet_v1_0.test
inputs = Inputs.CardsBoard.vs_majority_dominance_card_planes_binary
mcts_params = MctsParams(
  num_iters_per_turn=300,
  dirichlet_noise_ϵ=0.25,
  dirichlet_noise_α=1.0
)

# This is necessary since its not been defined in Just4Fun
GI.spec(g::Just4FunEnv) = gspec
#AlphaZero.convert_sample(gspec::Just4FunSpec, wp::SamplesWeighingPolicy, e::TrainingSample) = Architectures.ResNet_v1_0.convert_sample(gspec, wp, e)
GI.vectorize_state(gspec::Just4FunSpec, state) = inputs(gspec, state)

az_j4f_dir = dirname(dirname(pathof(AlphaZeroJust4Fun)))

include("$az_j4f_dir/scripts/utils/architecture.jl")

name = get_session_name(gspec, USE_GPU, parameters, architecture, inputs)
dir = UI.default_session_dir(name) # uses the directory julia was started in
!UI.valid_session_dir(dir) && throw("Session directory or session is invalid! ($dir)")
env = UI.load_env(dir)

join_remote_match(
  gspec, 
  AlphaZeroPlayer(env; timeout=nothing, mcts_params=mcts_params, use_gpu=USE_GPU),
  Player(RED)
)
