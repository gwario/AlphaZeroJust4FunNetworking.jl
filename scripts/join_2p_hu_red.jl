using Revise

using AlphaZeroJust4FunNetworking
using Just4Fun
using AlphaZero

include("logging.jl")
set_loglevel(Logging.Debug)

@info "Creating local game template..."
gspec = Just4FunSpec()
# This is necessary since its not been defined in Just4Fun
GI.spec(g::Just4FunEnv) = gspec

join_remote_match(gspec, Human(), Player(RED))