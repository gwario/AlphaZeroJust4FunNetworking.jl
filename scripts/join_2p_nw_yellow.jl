using Revise

using AlphaZeroJust4FunNetworking
using AlphaZeroJust4Fun
using Just4Fun
using AlphaZero
using AlphaZero: TrainingSample, GI, Benchmark, UI

include("logging.jl")
set_loglevel(Logging.Debug)

const USE_GPU = true

@info "Creating local game template..."
gspec = Just4FunSpec()

architecture = Architectures.ResNet_v1_0
parameters = Parameters.ResNet_v1_0.test # just to select the right session
inputs = Inputs.BoardOnly.vs_majority_dominance

# This is necessary since its not been defined in Just4Fun
GI.spec(g::Just4FunEnv) = gspec
GI.vectorize_state(gspec::Just4FunSpec, state) = inputs(gspec, state)

az_j4f_dir = dirname(dirname(pathof(AlphaZeroJust4Fun)))

include("$az_j4f_dir/scripts/utils/architecture.jl")

name = get_session_name(gspec, USE_GPU, parameters, architecture, inputs)
dir = UI.default_session_dir(name) # uses the directory julia was started in
!UI.valid_session_dir(dir) && throw("Session directory or session is invalid! ($dir)")
env = UI.load_env(dir)

join_remote_match(
    gspec,
    NetworkPlayer(Network.copy(env.bestnn, on_gpu=USE_GPU, test_mode=true)),
    Player(YELLOW)
)