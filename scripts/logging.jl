using Logging

set_loglevel(loglevel) = global_logger(ConsoleLogger(stderr, loglevel))
