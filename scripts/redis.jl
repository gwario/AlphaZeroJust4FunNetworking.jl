using Redis

@info "Connecting to redis server..."
conn = RedisConnection(; host="127.0.0.1", port=6379, db=0)
@info "Deleting all keys..."
flushall(conn)
disconnect(conn)