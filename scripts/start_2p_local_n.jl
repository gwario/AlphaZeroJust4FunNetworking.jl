using Revise

using AlphaZeroJust4FunNetworking
using Just4Fun
using AlphaZero

include("logging.jl")
set_loglevel(Logging.Debug)

const N_GAMES = UInt16(20)

@info "Creating local game..."
gspec = Just4FunSpec(nothing)

# This is necessary since its not been defined in Just4Fun
GI.spec(g::Just4FunEnv) = Just4FunSpec() #gspec # TODO: consider adding spec to evn again... then this is not an issue anymore

@info "Starting match of $N_GAMES games..."
provide_match(gspec, N_GAMES)