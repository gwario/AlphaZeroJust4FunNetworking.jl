module AlphaZeroJust4FunNetworking
using Jedis
using AlphaZero
using AlphaZero: GI
using Just4Fun
using JSON3
using StaticArrays
using SnoopPrecompile


@precompile_all_calls include("play.jl")
export interactive!

@precompile_all_calls include("structs.jl")
export ReceivingPlayer
export SendingPlayer
export PartialJust4FunEnv
@precompile_all_calls include("common.jl")
export connect_to_coordinator, disconnect_from_coordinator, write_game_state, read_game_state
@precompile_all_calls include("redis.jl")


@precompile_all_calls include("provider/common.jl")
@precompile_all_calls include("provider/local_redis.jl")
export start_local_game
export provide_game, provide_match

@precompile_all_calls begin
    include("player/receiver.jl")
    include("player/sender.jl")
    include("player/common.jl")
    include("player/redis.jl")
end
export join_remote_game, join_remote_match

export merge
end
