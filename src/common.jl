"""
connect_to_coordinator(; kwargs)
Opens a connection to to a game manager.
"""
function connect_to_coordinator end

"""
disconnect_from_coordinator(; kwargs)
Closes a connection to to a game manager.
"""
function disconnect_from_coordinator end

"""
read_game_state(; kwargs)::PartialJust4FunEnv
"""
function read_game_state end

"""
write_game_state(env::PartialJust4FunEnv; kwargs)
"""
function write_game_state end

"""
read_player_action(player::Player; kwargs)::CardsAction
"""
function read_player_action end

"""
write_player_action(action::CardsAction, player::Player; kwargs)
"""
function write_player_action end


const GameId = UInt16

const MatchId = UInt16

"""
merge!(local_game::Just4FunEnv, remote_game::PartialJust4FunEnv, local_player::Player)

Modifies the local complete game s.t. it adheres to the remote game's state.
Specifically, updates the local player's cards.
"""
function merge!(local_game::Just4FunEnv, remote_game::PartialJust4FunEnv, local_player::Player)
    #merged_game = GI.clone(local_game) # Clone is necessary otherwise the game that is passed as a parameter wont be modified
    @debug "Setting attribute from remote game on local game..."
    local_game.curplayer = remote_game.current_player
    for c_idx in 1:SIZE_HAND
        card_index = CartesianIndex(c_idx, Just4Fun.to_index(local_player))
        local_game.player_cards = setindex(local_game.player_cards, remote_game.player_cards[c_idx], card_index)
    end
    local_game.field_stones = SArray{Tuple{SIDE_LENGTH, SIDE_LENGTH, NUM_PLAYERS}, Stones}(remote_game.field_stones)
    local_game.player_stones = SVector{NUM_PLAYERS, Stones}(remote_game.player_stones)
    local_game.used_cards = copy(remote_game.used_cards)

    @debug "Updating action mask according the updated remote player's hand..."
    Just4Fun.update_action_mask!(local_game)
    @debug "Update of action mask done."
end

"""
merge!(local_game::Just4FunEnv, remote_player::Player, action::CardsAction)

Modifies the local complete game s.t. it adheres to the remote game's state.
Specifically, updates the local player's cards.
"""
function merge!(local_game::Just4FunEnv, remote_player::Player, action::CardsAction)
    #merged_game = GI.clone(local_game) # Clone is necessary otherwise the game that is passed as a parameter wont be modified TODO: this could be a problem
    # make shure the remote player has the cards for his action
    @debug "Making sure remote player has cards necessary for the give action..."
    for c_idx in 1:SIZE_HAND
        if c_idx <= length(action.cards)
            # put cards from action
            card = action.cards[c_idx]
        else
            # use filler, that produces a valid hand
            # since one exists 4 times, even if opponent played e.g. 1 1, this would only result in 4 1s which is valid
            card = CardValue(1)
        end
        local_game.player_cards = setindex(local_game.player_cards, card, CartesianIndex(c_idx, Just4Fun.to_index(remote_player)))
        #push!(local_game.used_cards, card)
    end
    @debug "Updating action mask according the updated remote player's hand..."
    Just4Fun.update_action_mask!(local_game)
    @debug "Update of action mask done."
end


"""
to_action(spec::Just4FunSpec, action_string::String)::CardsAction
action_string is of the format "c1 c2 ... -> fv"
"""
function to_action(spec::Just4FunSpec, action_message::String)::CardsAction
    action_string = strip(split(action_message, "->")[1])
    action = GI.parse_action(spec, action_string)
    return action
end

"""
to_game_id(game_id::String)::CardsAction
The game id
"""
function to_game_id(game_id_string::String)::GameId
    return parse(GameId, game_id_string)
end

"""
player_type(p::SendingPlayer{<: AbstractPlayer})::DataType{PT} where PT <: AbstractPlayer
"""
player_type(p::SendingPlayer{<: AbstractPlayer})::DataType{PT} where PT <: AbstractPlayer = p.player_type