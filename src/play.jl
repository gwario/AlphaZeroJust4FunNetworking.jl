function select_move(p::TwoPlayers, game, turn)
    if GI.white_playing(game)
        @debug "Yellow playing"
        return select_move(p.white, game, turn)
    else
        @debug "Red playing"
        return select_move(p.black, game, turn)
    end
end

"""
get_local_player(player::TwoPlayers)::Player

Returns the color of the local player.
"""
function get_local_player(player::TwoPlayers)::Player
    local_player = first(filter(p -> p isa SendingPlayer, [player.white, player.black]))
    return local_player.local_player
end

function interactive!(game::Just4FunEnv, player)
    try
        GI.render(game; player=get_local_player(player))
        turn = 0
        while !GI.game_terminated(game)
            @debug "Local game loop - selecting action by player $(player_name(game.curplayer))..."
            action = select_move(player, game, turn)
            @debug "Local game loop - selected action: $action"
            previous_player = game.curplayer
            @debug "Local game loop - playing action $action as $(player_name(game.curplayer))..."
            GI.play!(game, action)
            @debug "Local game loop - action applied"
            if previous_player == game.curplayer
                @error "Has to be next player's turn after applying previous player's action!"
            end
            turn += 1
        end
    catch e
        @show e
        @error "Local game loop - Error!" e
        return
    end
end