"""
join_remote_game(player::AbstractPlayer, color::Player)

Join a remote game with a player as a color.
"""
function join_remote_game end

"""
join_remote_match(gspec::Just4FunSpec, player_type::AbstractPlayer, player::Player)

Join a remote match (of multiple games) with a player as a color.
"""
function join_remote_match end