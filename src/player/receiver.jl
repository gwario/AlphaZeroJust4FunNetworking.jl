function select_move(p::ReceivingPlayer, local_game, turn)
  local_player_name = player_name(p.local_player)
  remote_player_name = player_name(p.remote_player)

  @info "Waiting for player $remote_player_name's action..."
  opponent_action = await_opponent_action(p.gspec, p.local_player)
  @info "$remote_player_name (opponent) played $(GI.action_string(p.gspec, opponent_action))"
  
  previous_player = local_game.curplayer
  # add the cards opponent played to his hand
  @info "Syncronizing game environment..."
  merge!(local_game, p.remote_player, opponent_action)
  local_game.curplayer = previous_player
  @info "Syncronizition done."
  if previous_player != local_game.curplayer
    @error "Current player has to be the same in local and remote game state!"
  end

  @info "Selecting remote opponent's move."
  return opponent_action
end
