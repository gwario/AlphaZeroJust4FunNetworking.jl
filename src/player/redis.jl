"""
join_remote_game(gspec::Just4FunSpec, player_type::AbstractPlayer, player::Player)

deprecated:
"""
function join_remote_game(gspec::Just4FunSpec, player_type::AbstractPlayer, player::Player)

    local_genv = @timev "environment" GI.init(gspec)

    connect_to_coordinator()

    @info "Synchronizing game environment..."
    pgame = read_game_state()
    @debug "Merge"
    merge!(local_genv, pgame, player)
    
    @info "Creating players..."
    local_player = SendingPlayer(gspec, local_genv, player, player_type)
    remote_player = ReceivingPlayer(gspec, local_genv, player)

    @info "Playing $(nameof(typeof(local_player.actual_player))) ($(player_name(player))) vs $(nameof(typeof(remote_player)))"

    @info "Joining network game."
    AlphaZeroJust4FunNetworking.interactive!(local_genv, TwoPlayers(
        player == Player(YELLOW) ? local_player : remote_player,
        player == Player(YELLOW) ? remote_player : local_player
    ))

    if GI.game_terminated(local_genv)
        GI.render(local_genv)
        local_player_won = local_genv.winner == local_player.local_player
        @info "Game terminated ($(local_genv.state))."
        @info "Player $(player_name(local_genv.winner)) won ($(local_player_won ? "you" : "opponent"))!"
    end

    disconnect_from_coordinator()
end

function join_remote_match(gspec::Just4FunSpec, player_type::AbstractPlayer, player::Player)

    connect_to_coordinator()

    @info "Creating local game environment..."
    global local_genv = @timev "environment" GI.init(gspec)

    @info "Creating players..."
    local_player = SendingPlayer(gspec, local_genv, player, player_type)
    remote_player = ReceivingPlayer(gspec, local_genv, player)

    @info "Waiting for game..."
    game_start_client = Client(host=get_global_client().host, port=get_global_client().port)
    @async subscribe(game_start_topic(); stop_fn=is_cancellation_message, client=game_start_client) do msg
        try
            @debug "<play_remote_game>"
            play_remote_game(local_player, remote_player, msg)
            @debug "</play_remote_game>"
        catch e
            @show e
            @error "Error during play_remote_game!" e
        end
    end

    await_match_end()
    @info "Match ended."
    disconnect!(game_start_client)

    disconnect_from_coordinator()
end

function play_remote_game(local_player, remote_player, msg)
    @show msg
    #@show msg[end-1] == game_start_topic()
    #@show tryparse(Int64, msg[end]) !== nothing
    if is_game_start_message(msg)
        @info "is_game_start_message"
        game_id = to_game_id(msg[end])
        #play_game(local_genv, local_player, remote_player, to_game_id(msg))
        @info "Joining network game $game_id."
        @info "Synchronizing game environment..."
        pgame = read_game_state()
        merge!(local_genv, pgame, local_player.local_player)
        # reset game env further
        local_genv.state = in_progress
        local_genv.winner = Player(0)
        local_genv.action_indices = Vector{Int64}()
        
        # TODO: reset if player with mcts

        @info "Playing $(nameof(typeof(local_player.actual_player))) ($(player_name(local_player.local_player))) vs $(nameof(typeof(remote_player)))"
        AlphaZeroJust4FunNetworking.interactive!(local_genv, TwoPlayers(
            local_player.local_player == Player(YELLOW) ? local_player : remote_player,
            local_player.local_player == Player(YELLOW) ? remote_player : local_player
        ))

        if GI.game_terminated(local_genv)
            #GI.render(local_genv)
            local_player_won = local_genv.winner == local_player.local_player
            @info "Game terminated ($(local_genv.state))."
            @info "$(local_player_won ? "You" : "Opponent") won!"
            print_points(local_genv)
            print_game_result(local_genv)
        end
    end
end