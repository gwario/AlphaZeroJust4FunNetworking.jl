"""
select_move(p::SendingPlayer, local_game, turn)

1. Updates game according to the current remove game state.
2. Dispatches to player_type's implementation.
3. Sends the selected move to the game provider.
"""
function select_move(p::SendingPlayer{<:AbstractPlayer}, local_game, turn)
    local_player_type = typeof(p.actual_player)

    @debug "Syncronizing game environment..."
    remote_game = read_game_state()

    previous_player = local_game.curplayer
    # if its player's turn, modify local game state according to remove
    merge!(local_game, remote_game, p.local_player)
    @debug "Syncronization done."
    local_game.curplayer = previous_player
    #GI.render(local_game; player=p.local_player)

    # call players select move(might be human, ...)
    @info "Dispatching select_move to $local_player_type..."
    a = AlphaZero.select_move(p.actual_player, local_game, turn)

    @info "Submitting action $(GI.action_string(p.gspec, a)) to coordinator..."
    submit_action(p, a)

    return a
end