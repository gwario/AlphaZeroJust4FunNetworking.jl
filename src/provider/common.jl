"""
provide_game(genv)

Start syncing game states with players.
"""
function provide_game end

"""
on_player_action(genv::Just4FunEnv, action::CardsAction; kwargs)

Applies the action to the game.
"""
function on_player_action end
