#"""
#on_player_action(genv::Just4FunEnv, action::CardsAction)
#
#Callback player actions.
#"""
#function on_player_action(genv::Just4FunEnv, action::CardsAction)
#    try
#        current_player = genv.curplayer
#    
#        @info "Player $(player_name(current_player)) played $action"
#    
#        # play action
#        GI.play!(genv, action)
#    
#        GI.render(genv, debug=true)
#    
#        # write game state
#        @info "Updated game environment..."
#        write_game_state(genv)
#    
#        @info "Broadcasting player action..."
#        broadcast_player_action(current_player, action, genv)
#    catch err
#        @show err
#        @error "Error in on_player_action"
#    end
#end

"""
provide_match(gspec::Just4FunSpec, n_games::UInt16)

Provides multiple games as a match.
"""
function provide_match(gspec::Just4FunSpec, n_games::GameId)

    connect_to_coordinator()

    broadcast_match_start()

    global genv = nothing
    
    for game_idx in 1:n_games
        # TODO: consider randomizing+resetting the game env to increase performance
        global genv = @timev "environment" GI.init(gspec)
        @info "Providing $game_idx game..."
        provide_game!(genv, GameId(game_idx))
    
        # TODO: collect results from genv
    end
    
    # TODO: print statistics

    broadcast_match_end()
    
    disconnect_from_coordinator()
end



"""
provide_game!(genv::Just4FunEnv, game_id::GameId)

Starts a locally generated game of a match.
"""
function provide_game!(genv::Just4FunEnv, game_id::GameId)
        
    @info "Player $(player_name(genv.curplayer)) will start."
    @info "Starting to listen for player actions..."
    player_action_client = Client(host=get_global_client().host, port=get_global_client().port)
    @async subscribe(player_action_topic(); stop_fn=is_cancellation_message, client=player_action_client) do msg
        @debug "onPlayerAction"
        @show msg
        
        #some how this callback does not get called more than once and thus does not handle 
        #also variable manupulation within the task is apparently weird.
        #if we read the most recent full game state here, manipulate it and save it, it could work... 
        #    but whats the easiest way to do it... redis as well or maybe julia async programming!?!?

        #on_player_action(genv, to_action(GI.spec(genv), msg))
        action = to_action(GI.spec(genv), msg[end])
        previous_player = genv.curplayer
        action_string = GI.action_string(GI.spec(genv), action)
        @info "Player $previous_player played $action_string"
        # play action
        GI.play!(genv, action)
        GI.render(genv, debug=true)
        if previous_player == genv.curplayer
            @error "Has to be next player's turn after applying previous player's action!"
        end
        # write game state
        @info "Updated game environment..."
        write_game_state(genv)
    
        @info "Broadcasting player action..."
        broadcast_player_action(previous_player, action, genv)
        @info "after on_player_action"
    end
    wait_until_subscribed(player_action_client)

    GI.render(genv, debug=true)
    @info "Publishing the initial game state..."
    write_game_state(genv)

    # FIXME: the second games starts initiatied by coordinaor but the agents do not realize the game new game has started and still thing the previous game has ended (with the initial game state of the new game)
    
    @info "Signalling game start to player..."
    signal_game_start(game_id)

    @info "Waiting for game $game_id to finish..."
    while !GI.game_terminated(genv)
        @info "Current player: $(player_name(genv.curplayer))"
        sleep(1)
    end
    disconnect!(player_action_client)
    @info "Game $game_id finished."
    publish_cancellation_message(player_action_topic())

    @info "Game $game_id results:"
    print_points(genv)
    print_game_result(genv)
end
