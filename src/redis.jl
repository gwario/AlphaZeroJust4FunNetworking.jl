"""
connect_to_coordinator(; [host::String, port::Int, password::String])

Opens a connection to to a game coordinator.

sets the global module variables connection and subscription
"""
function connect_to_coordinator(;
    host::String = "127.0.0.1",
    port::Int = 6379
)
    
    if isnothing(get_global_client())
        @info "Connecting to redis server..."
        set_global_client(Client(host=host, port=port))
    end
    @info "Connection established"
end

"""
disconnect_from_coordinator()

Closes a connection to to a game coordinator.

Disconnects from the gloabal module variables connection and subscription
"""
function disconnect_from_coordinator()
    @info "Disconnecting from redis server..."
    disconnect!(get_global_client())
    @info "Disconnected"
end

"""
player_action_topic()

Notifies the provider about actions - Used by the players to share actions with the provider.
"""
player_action_topic() = "player/action"

"""
opponent_actions_list(player::Player)

Contains a space separated strings of the cards played by the current player. - Used by the provider to share actions with players.
"""
opponent_actions_list(player::Player) = "player/$(Int(player))/receive_actions"

"""
game_state_hash()

Has fields for the game state values. - Used by the provider to share actions with players.
"""
game_state_hash() = "game/state"

"""
match_topic()

State of the match - Used by the player's to wait until the match ends.
"""
match_topic() = "match/event"

"""
game_start_topic()

Topic
Used by the player's to start an interactive session for a given game.
"""
game_start_topic() = "game/event/start"


"""
signal_game_start(game_id::GameId)

Signals the start of a new game to players.
"""
function signal_game_start(game_id::GameId)
    client = Client(host=get_global_client().host, port=get_global_client().port)
    publish(game_start_topic(), "$game_id"; client=client)
    disconnect!(client)
end

"""
broadcast_player_action(current_player::Player, action::CardsAction, genv::Just4FunEnv)
"""
function broadcast_player_action(current_player::Player, action::CardsAction, genv::Just4FunEnv)
    # TODO: modify for multiplaye
    gspec = GI.spec(genv)
    player_who_acted_name = player_name(current_player)
    num_players = GI.two_players(gspec) ? 2 : 1
    client = Client(host=get_global_client().host, port=get_global_client().port)
    # send to other players
    for p_idx in 1:num_players
        player = Player(p_idx)
        player == current_player && continue # skipt the player how made the move
        action = GI.action_string(gspec, action)
        @info "Sending $player_who_acted_name's action ($action) to player $(player_name(player))"
        lpush(opponent_actions_list(player), action; client=client)
    end
    disconnect!(client)
end

"""
broadcast_match_start()
"""
function broadcast_match_start()
    client = Client(host=get_global_client().host, port=get_global_client().port)
    publish(match_topic(), "started"; client=client)
    disconnect!(client)
end

"""
broadcast_match_end()
"""
function broadcast_match_end()
    client = Client(host=get_global_client().host, port=get_global_client().port)
    publish(match_topic(), "ended"; client=client)
    disconnect!(client)
end


"""
await_match_end()

Waits for the match to end.
"""
function await_match_end()
    match_client = Client(host=get_global_client().host, port=get_global_client().port) 
    subscribe(match_topic(); stop_fn=is_cancellation_message, client=match_client) do msg
        @debug "onMatchSignal"
        @show msg
    end
    disconnect!(match_client)
end

"""
submit_action(p::SendingPlayer, action::CardsAction)

Submits an action to the game provider.
"""
function submit_action(p::SendingPlayer, action::CardsAction)
    client = Client(host=get_global_client().host, port=get_global_client().port)
    publish(player_action_topic(), GI.action_string(p.gspec, action); client=client)
    disconnect!(client)
end

"""
await_opponent_action(gspec::Just4FunSpec, local_player::::Player)::CardsAction

Waits for a player action to be available.
"""
function await_opponent_action(gspec::Just4FunSpec, local_player::Player)::CardsAction
    client = Client(host=get_global_client().host, port=get_global_client().port)
    action_string = brpop(opponent_actions_list(local_player), 0; client=client)
    disconnect!(client)
    @show action_string 
    action = to_action(gspec, action_string[2])
    return action
end

"""
read_game_state()::PartialJust4FunEnv

Reads the game state.
"""
function read_game_state()::PartialJust4FunEnv
    #@debug "Creating temporary client..."
    client = Client(host=get_global_client().host, port=get_global_client().port)
    #@debug "Temporary client created."
    
    # get the relevant fields
    #@debug "Reading remote used cards..."
    used_cards      = JSON3.read(hget(game_state_hash(), "used_cards"; client=client), Cards)
    #@debug "Remote used cards are: $used_cards"
    
    #@debug "Reading remote player cards..."
    player_cards    = JSON3.read(hget(game_state_hash(), "player_cards"; client=client), Cards)
    #@debug "Remote player cards are: $player_cards"

    #@debug "Reading remote field stones..."
    field_stones    = reshape(JSON3.read(hget(game_state_hash(), "field_stones"; client=client), Array{Stones}), 6, 6, 2)
    #@debug "Remote player field stones receieved"

    #@debug "Reading remote player stones..."
    player_stones   = JSON3.read(hget(game_state_hash(), "player_stones"; client=client), Vector{Stones})
    #@debug "Remote player stones are: $player_stones"

    #@debug "Reading remote current player..."
    current_player  = JSON3.read(hget(game_state_hash(), "current_player"; client=client), Player)
    #@debug "Remote current player: $current_player"

    # does not work since deserialization to struct is performed with field stones being a on dim array but a 3 dim is required
    #state = JSON3.read(hget(game_state_hash(), "state"; client=client), PartialJust4FunEnv)

    #@debug "Disconnecting temporary client..."
    disconnect!(client)
    #@debug "Temporary client discennected."
    return PartialJust4FunEnv(used_cards, player_cards, field_stones, player_stones, current_player)
end

"""
write_game_state(env::PartialJust4FunEnv)

Writes the game state.
"""
function write_game_state(genv::Just4FunEnv)
    client = Client(host=get_global_client().host, port=get_global_client().port)
    partial_game_env = PartialJust4FunEnv(genv)
    # set relevant fields
    hset(game_state_hash(), "used_cards", JSON3.write(partial_game_env.used_cards); client=client)
    hset(game_state_hash(), "player_cards", JSON3.write(partial_game_env.player_cards); client=client)
    hset(game_state_hash(), "field_stones", JSON3.write(partial_game_env.field_stones); client=client)
    hset(game_state_hash(), "player_stones", JSON3.write(partial_game_env.player_stones); client=client)
    hset(game_state_hash(), "current_player", JSON3.write(partial_game_env.current_player); client=client)
    # Does nto work, see read function
    #hset(game_state_hash(), "state", JSON3.write(partial_game_env); client=client)
    
    disconnect!(client)
end

"""
publish_cancellation_message(topic)

Publishes a message to make subscribers cancel their subscription.
"""
publish_cancellation_message(topic) = publish(topic, "cancel subscription")

"""
is_cancellation_message(msg)

Retruns true if the subsciption should be canceled.
"""
is_cancellation_message(msg) = msg[end] == "cancel subscription"

"""
is_game_start_message(msg)

Retruns true if it is a game start message.
"""
is_game_start_message(msg) = msg[end-1] == game_start_topic() && tryparse(Int64, msg[end]) !== nothing