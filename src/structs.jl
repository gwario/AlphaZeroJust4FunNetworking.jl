struct PartialJust4FunEnv
    used_cards::Cards
    player_cards::Cards
    field_stones::Array{UInt8, 3}
    player_stones::Array
    current_player::Player
    
    function PartialJust4FunEnv(genv::Just4FunEnv)
        new(
            genv.used_cards,
            curplayercards(genv),
            genv.field_stones,
            genv.player_stones,
            genv.curplayer
        )
    end

    function PartialJust4FunEnv(
        used_cards::Cards, player_cards::Cards, field_stones::Array{UInt8, 3},
        player_stones::Vector{Stones}, current_player::Player
    )
    
        new(
            used_cards,
            player_cards, #curplayercards(genv),
            field_stones,
            player_stones,
            current_player
        )
    end
end

"""
ReceivingPlayer <: AbstractPlayer

A player that retrieves its moves from a game coordinator.
"""
struct ReceivingPlayer <: AbstractPlayer
    local_player::Player
    remote_player::Player

    pgenv::PartialJust4FunEnv

    gspec::Just4FunSpec
    genv::Just4FunEnv

    function ReceivingPlayer(
        gspec::Just4FunSpec, genv::Just4FunEnv,
        local_player::Player
    )

        new(
            local_player == Player(YELLOW) ? Player(YELLOW) : Player(RED), #local_player::Player
            local_player == Player(YELLOW) ? Player(RED) : Player(YELLOW), #remote_player::Player
            PartialJust4FunEnv(genv), #pgenv::PartialJust4FunEnv
            gspec, #gspec::Just4FunSpec
            genv, #genv::Just4FunEnv
        )
  end
end

"""
SendingPlayer <: AbstractPlayer

A player that sends its moves to a game coordinator.
"""
struct SendingPlayer{PT <: AbstractPlayer} <: AbstractPlayer
    local_player::Player
    remote_player::Player

    pgenv::PartialJust4FunEnv
  
    gspec::Just4FunSpec
    genv::Just4FunEnv
    actual_player::PT

    function SendingPlayer(
        gspec::Just4FunSpec, genv::Just4FunEnv,
        local_player::Player, player_type::PT
    )::SendingPlayer{PT} where {PT}

        new{PT}(
            local_player == Player(YELLOW) ? Player(YELLOW) : Player(RED), #local_player::Player
            local_player == Player(YELLOW) ? Player(RED) : Player(YELLOW), #remote_player::Player
            PartialJust4FunEnv(genv), #pgenv::PartialJust4FunEnv
            gspec, #gspec::Just4FunSpec
            genv, #genv::Just4FunEnv
            player_type #actual_player::PT
        )
    end
end